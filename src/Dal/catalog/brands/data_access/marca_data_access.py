from typing import Dict, List

from Dal.catalog.brands.entities.marca_entity import MarcaEntity


class MarcaDataAccess:
    def __init__(self):
        self.entity = MarcaEntity()

    def migrate_table(self):
        self.entity.create_table()

    def query(self) -> MarcaEntity:
        return self.entity

    @staticmethod
    def filtering(filters: {} = None) -> List:
        filters = filters or dict()

        filterExpression: List = [1 == 1]

        if 'nome' in filters and filters['nome'] != "":
            filterExpression.append(MarcaEntity.nome == filters['nome'])

        return filterExpression

    def create_marca(self, new_marca: Dict) -> bool:
        return self.query().insert_many([new_marca]).execute() > 0

    def update_marca(self, update_marca: Dict, id_marca: int) -> bool:
        query = self.query().update(**update_marca).where(MarcaEntity.id == id_marca)
        return query.execute() > 0

    def list_all_marca(self, filters: {} = None) -> List:
        query = self.query().select().where(*self.filtering(filters)).dicts()
        return list(query)

    def get_marca(self, id_marca: int) -> MarcaEntity:
        query = self.query().select().where(MarcaEntity.id == id_marca).dicts()
        count = query.count()
        if count > 0:
            return query.get()
        return None

    def delete_marca(self, id_marca: int) -> bool:
        return self.query().delete().where(MarcaEntity.id == id_marca).execute() > 0
