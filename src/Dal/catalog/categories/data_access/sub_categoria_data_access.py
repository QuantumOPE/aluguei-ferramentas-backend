from typing import Dict, List

from Dal.catalog.categories.entities.sub_categoria_entity import SubCategoriaEntity


class SubCategoriaDataAccess:
    def __init__(self):
        self.entity = SubCategoriaEntity()

    def migrate_table(self):
        self.entity.create_table()

    def query(self) -> SubCategoriaEntity:
        return self.entity

    @staticmethod
    def filtering(filters: {} = None) -> List:
        filters = filters or dict()

        filterExpression: List = [1 == 1]

        if 'descricao' in filters and filters['descricao'] != "":
            filterExpression.append(SubCategoriaEntity.descricao == filters['descricao'])

        return filterExpression

    def create_sub_categoria(self, new_sub_categoria: Dict) -> bool:
        return self.query().insert_many([new_sub_categoria]).execute() > 0

    def update_sub_categoria(self, update_sub_categoria: Dict, id_sub_categoria: int) -> bool:
        query = self.query().update(**update_sub_categoria).where(SubCategoriaEntity.id == id_sub_categoria)
        return query.execute() > 0

    def list_all_sub_categoria(self, filters: {} = None) -> List:
        query = self.query().select().where(*self.filtering(filters)).dicts()
        return list(query)

    def get_sub_categoria(self, id_sub_categoria: int) -> SubCategoriaEntity:
        query = self.query().select().where(SubCategoriaEntity.id == id_sub_categoria).dicts()
        count = query.count()
        if count > 0:
            return query.get()
        return None

    def delete_sub_categoria(self, id_sub_categoria: int) -> bool:
        return self.query().delete().where(SubCategoriaEntity.id == id_sub_categoria).execute() > 0
