from typing import Dict, List

from Dal.catalog.categories.entities.departamento_entity import DepartamentoEntity


class DepartamentoDataAccess:
    def __init__(self):
        self.entity = DepartamentoEntity()

    def migrate_table(self):
        self.entity.create_table()

    def query(self) -> DepartamentoEntity:
        return self.entity

    @staticmethod
    def filtering(filters: {} = None) -> List:
        filters = filters or dict()

        filterExpression: List = [1 == 1]

        if 'descricao' in filters and filters['descricao'] != "":
            filterExpression.append(DepartamentoEntity.descricao.contains(filters['descricao']))

        if 'nome' in filters and filters['nome'] != "":
            filterExpression.append(DepartamentoEntity.nome.contains(filters['nome']))

        return filterExpression

    def create_departamento(self, new_departamento: Dict) -> bool:
        return self.query().insert_many([new_departamento]).execute() > 0

    def update_departamento(self, update_departamento: Dict, id_departamento: int) -> bool:
        query = self.query().update(**update_departamento).where(DepartamentoEntity.id == id_departamento)
        return query.execute() > 0

    def list_all_departamento(self, filters: {} = None) -> List:
        query = self.query().select().where(*self.filtering(filters)).dicts()
        return list(query)

    def get_departamento(self, id_departamento: int) -> DepartamentoEntity:
        query = self.query().select().where(DepartamentoEntity.id == id_departamento).dicts()
        count = query.count()
        if count > 0:
            return query.get()
        return None

    def delete_departamento(self, id_departamento: int) -> bool:
        return self.query().delete().where(DepartamentoEntity.id == id_departamento).execute() > 0
