from peewee import *

from Dal._core.pewee_base_entity import PeeweeBaseEntity
from Dal.catalog.categories.entities.categoria_entity import CategoriaEntity


class SubCategoriaEntity(PeeweeBaseEntity):
    class Meta:
        db_table = 'tb_sub_categoria'

    idCategoria: ForeignKeyField = ForeignKeyField(CategoriaEntity, db_column='idCategoria')

    descricao: CharField = CharField(unique=False, null=True)



