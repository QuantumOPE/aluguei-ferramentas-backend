from typing import Dict, List, Tuple

from Dal.identity.usuarios.entities.usuario_entity import UsuarioEntity


class UsuarioDataAccess:
    def __init__(self):
        self.entity = UsuarioEntity()

    def migrate_table(self):
        self.entity.create_table()

    def query(self) -> UsuarioEntity:
        return self.entity

    @staticmethod
    def filtering(filters: {} = None) -> List:
        filters = filters or dict()

        filterExpression: List = [1 == 1]

        if 'flagAtivo' in filters and filters['flagAtivo'] != "":
            filterExpression.append(UsuarioEntity.flagAtivo == filters['flagAtivo'])

        if 'nome' in filters and filters['nome'] != "":
            filterExpression.append(UsuarioEntity.nome.contains(filters['nome']))

        if 'sobreNome' in filters and filters['sobreNome'] != "":
            filterExpression.append(UsuarioEntity.sobreNome.contains(filters['sobreNome']))

        if 'numeroCpf' in filters and filters['numeroCpf'] != "":
            filterExpression.append(UsuarioEntity.numeroCpf.contains(filters['numeroCpf']))

        if 'email' in filters and filters['email'] != "":
            filterExpression.append(UsuarioEntity.email.contains(filters['email']))

        if 'nomeUsuario' in filters and filters['nomeUsuario'] != "":
            filterExpression.append(UsuarioEntity.nomeUsuario.contains(filters['nomeUsuario']))

        if 'senhaUsuario' in filters and filters['senhaUsuario'] != "":
            filterExpression.append(UsuarioEntity.senhaUsuario.contains(filters['senhaUsuario']))

        if 'idUsuarioPerfilAcesso' in filters and filters['idUsuarioPerfilAcesso'] != "":
            filterExpression.append(UsuarioEntity.idUsuarioPerfilAcesso.contains(filters['idUsuarioPerfilAcesso']))

        if 'idContatoTelefone' in filters and filters['idContatoTelefone'] != "":
            filterExpression.append(UsuarioEntity.idContatoTelefone.contains(filters['idContatoTelefone']))

        return filterExpression

    def verificar_duplicidade(self, usuario: Dict) -> Tuple[bool, str]:
        duplicaded_email_usuario = usuario["email"] if "email" in usuario else ""

        query = (self.query()
                     .select()
                     .where((UsuarioEntity.email == duplicaded_email_usuario))
                     .dicts())

        count = query.count()

        if count > 0:
            return True, "email já cadastrados no sistema"

        return False, ""

    def create_usuario(self, usuario: Dict) -> bool:
        return self.query().insert_many([usuario]).execute() > 0

    def update_usuario(self, usuario: Dict, id_usuario: int) -> bool:
        query = self.query().update(**usuario).where(UsuarioEntity.id == id_usuario)
        return query.execute() > 0

    def list_all_usuario(self, filters: {} = None):
        query = self.query().select().where(*self.filtering(filters)).dicts()
        return list(query)

    def get_usuario(self, id_usuario: int):
        query = self.query().select().where(UsuarioEntity.id == id_usuario).dicts()
        count = query.count()
        if count > 0:
            return query.get()
        return None

    def delete_usuario(self, id_usuario: int) -> int:
        return self.query().delete().where(UsuarioEntity.id == id_usuario).execute() > 0