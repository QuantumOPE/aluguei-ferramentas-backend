from datetime import datetime


class ProdutoDto:
    id: int = None
    flagAtivo: bool = None
    dtCadastro: datetime = None

    idMarca: int = None
    nome: int = None
    caracteristicas: str = None
    descricao: str = None
    urlFotoPrincipal: str = None
