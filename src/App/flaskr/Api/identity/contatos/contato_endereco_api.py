from typing import Tuple, Any

from flask import Blueprint, jsonify, request

from flask_restful import Resource

from App.flaskr.Api._core.default_response_dto import DefaultResponseDto
from Dal.identity.contatos.data_access.contato_endereco_data_access import ContatoEnderecoDataAccess
from Dal.identity.contatos.dtos.contato_endereco_dto import ContatoEnderecoDto
from Infra.Util.ObjectExtensions import ObjectExtensions


class ContatoEnderecoApi(Resource):
    uri: str = '/api/v1/identity/address'

    def __init__(self):
        self.enderecos_dal = ContatoEnderecoDataAccess()
        self.args = request.args
        self.response: DefaultResponseDto = DefaultResponseDto()
        self.response.arguments = self.args

    def get(self, id_contact_address: int = 0):
        if id_contact_address > 0:
            return self.__get_single(id_contact_address)
        return self.__get_all()

    def __get_single(self, id_contact_address: int) -> Tuple[Any, int]:
        user = self.enderecos_dal.get_endereco(id_contact_address)

        if user is not None:
            self.response.messages = ["registros encontrados com sucesso id: " + str(id_contact_address)]
            self.response.results = [user]

            return self.response.to_dictionary(), 200

        self.response.messages = ["nenhum registro encontrado com o id: " + str(id_contact_address)]
        return self.response.to_dictionary(), 404

    def __get_all(self) -> Tuple[Any, int]:
        allcompanies_list = self.enderecos_dal.list_all_endereco(self.args)
        if len(allcompanies_list) > 0:
            self.response.messages = ["registros encontrado com sucesso"]
            self.response.results = allcompanies_list

            return self.response.to_dictionary(), 200

        self.response.messages = ["nenhum registro encontrado"]
        return self.response.to_dictionary(), 404

    def post(self) -> Tuple[Any, int]:
        if request.get_json() is None:
            self.response.messages.append("dados enviados não condizentes")
            return self.response.to_dictionary(), 403

        data_mapping = ObjectExtensions.from_dict(ContatoEnderecoDto, request.get_json())
        data_dict = ObjectExtensions.to_dictionary(data_mapping, True)

        flag_cadastrado = self.enderecos_dal.create_endereco(data_dict)

        if flag_cadastrado:
            self.response.messages = ["registro cadastrado com sucesso"]
            return self.response.to_dictionary(), 201

        self.response.messages = ["algo errado ocorreu no processo de cadastro"]
        return self.response.to_dictionary(), 404

    def put(self, id_contact_address: int) -> Tuple[Any, int]:
        if request.get_json() is None:
            self.response.messages.append("dados enviados não condizentes")
            return self.response.to_dictionary(), 403

        data_mapping = ObjectExtensions.from_dict(ContatoEnderecoDto, request.get_json())
        data_dict = ObjectExtensions.to_dictionary(data_mapping, True)

        flag_atualizado = self.enderecos_dal.update_endereco(data_dict, id_contact_address)

        if flag_atualizado:
            self.response.messages.append("registro atualizado com sucesso")
            return self.response.to_dictionary(), 201

        self.response.messages.append("Registro não encontrado")
        return self.response.to_dictionary(), 404

    def delete(self, id_contact_address: int) -> Tuple[Any, int]:
        flag_excluido = self.enderecos_dal.delete_endereco(id_contact_address)

        if flag_excluido:
            self.response.messages.append("registro excluido com sucesso")
            return self.response.to_dictionary(), 201

        self.response.messages.append("registro não encontrado")
        return self.response.to_dictionary(), 404
