from datetime import datetime


class CategoriaDto:
    id: int = None
    flagAtivo: bool = None
    dtCadastro: datetime = None

    idDepartamento: int = None
    descricao: str = None


