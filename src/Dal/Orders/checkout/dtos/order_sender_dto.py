class OrderSenderDto:
    name: str = None
    telePhoneAreaCode: str = None
    phoneNumber: str = None
    email: str = None
