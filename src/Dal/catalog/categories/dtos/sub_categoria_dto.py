from datetime import datetime

from peewee import *

from Dal._core.pewee_base_entity import PeeweeBaseEntity
from Dal.catalog.categories.entities.categoria_entity import CategoriaEntity


class SubCategoriaEntity(PeeweeBaseEntity):
    id: int = None
    flagAtivo: bool = None
    dtCadastro: datetime = None

    idCategoria: int = None

    descricao: str = None


