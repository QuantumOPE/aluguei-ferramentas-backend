from functools import wraps
from typing import Dict, List

from Dal.catalog.products.entities.produto_entity import ProdutoEntity


class ProdutoDataAccess:
    def __init__(self):
        self.productEntity = ProdutoEntity()

    def migrate_table(self):
        self.productEntity.create_table()

    def query(self) -> ProdutoEntity:
        return self.productEntity

    @staticmethod
    def filtering(filters: {} = None) -> List:
        filters = filters or dict()

        filterExpression: List = [1 == 1]

        if 'nome' in filters and filters['nome'] != "":
            filterExpression.append(ProdutoEntity.nome.contains(filters['nome']))

        if 'descricao' in filters and filters['descricao'] != "":
            filterExpression.append(ProdutoEntity.descricao.contains(filters['descricao']))

        '''if 'idMarca' in filters and filters['idMarca'] != "":
            filterExpression.append(ProdutoEntity.idMarca == int(filters['descricao']))
        '''

        if 'caracteristicas' in filters and filters['caracteristicas'] != "":
            filterExpression.append(ProdutoEntity.caracteristicas.contains(filters['caracteristicas']))

        return filterExpression

    def create_product(self, product: Dict) -> bool:
        return self.query().insert_many([product]).execute() > 0

    def update_product(self, product: Dict, id_product: int) -> bool:
        query = self.query().update(**product).where(ProdutoEntity.id == id_product)
        return query.execute() > 0

    def list_all_product(self, filters: {} = None):
        query = self.query().select().where(*self.filtering(filters)).dicts()
        return list(query)

    def get_product(self, id_product: int):
        query = self.query().select().where(ProdutoEntity.id == id_product).dicts()
        count = query.count()
        if count > 0:
            return query.get()
        return None

    def delete_product(self, id_product: int) -> int:
        return self.query().delete().where(ProdutoEntity.id == id_product).execute() > 0
