from datetime import datetime


class UsuarioDto:
    id: int = None
    flagAtivo: bool = None
    dtCadastro: datetime = None

    nome: str = None
    sobreNome: str = None
    numeroCpf: str = None
    email: str = None
    nomeUsuario: str = None
    senhaUsuario: str = None

    idUsuarioPerfilAcesso: int = None
    idContatoTelefone: int = None
