# Randit - Aluguel de ferramentas e equipamentos #

Projeto iniciado com visão a conclusão de disciplina de desenvolvimento e inovação na Faculdade,
desenvolvido em Python/Flask.

---

## Contributions and tutorials

Tutorial para contribuição no projeto

### Starting

1. Requisitos: 
```
    Python 3.8
    Click 7.0
    Flask 1.1.1
    Jinja2 2.11.1
    MarkupSafe 1.1.1
    PyMySQL	0.9.3
    Werkzeug 1.0.0
    itsdangerous 1.1.0
    peewee	3.13.1
    pip	19.2.3
    setuptools 45.2.0
```

2. Dentro da pasta __src/App/main.py__ do projeto executar o comando via terminal: *python3 main.py*
3. Aplicação estará rodando em **http://localhost:5080/**

## Organização de Branchs para contribuição ##

O projeto de back-end usará o esquema de gitflow, consiste em separa features de hotfixes

### Feature

1. Clonar o repositório e sacar **origin/development**;
2. Criar uma branch com nome da feature seguindo o padrão: 'development/nome-feature' como 'development/tela-edicao-dados-cliente';
3. Desenvolver a feature;
4. mesclar a feature com a **development**;
5. Realizar o pull-request da **development** para **origin/development**;

### HotFixes

1. Clonar o repositório e sacar **origin/master**;
2. Criar uma branch com nome da hotfix seguindo o padrão: 'hotfix/versao/numero-nome-hotfix' como 'hotfix/v0.1/001-ajustes-campos-usuario';
3. Resolver o bug;
4. mesclar a hotfix com a **master**;
6. Realizar testes;
7. Subir a sua hotfix para a origin
8. Realizar o pull-request da **master** para **origin/master**;

---

## Structure
A arquitetura do projeto contempla a arquitetura dividida em camadas de aplicação, sendo: 
```
   App   -> Aplicação Web MVC e RestFul com Flask e Jinja
   Dal   -> Camada de Acesso a dados, organização de modelos e entidades 
   Bll   -> Camada Processos que possuem regras e validações de negócio
   Infra -> Camada de apoio à aplicação, com metodos helpers de conversões e afins 
```

## Documents

A documentação e o processo de desenvolvimento está sendo controlado no [Trello](https://trello.com/b/rqx6oav2/projeto-aluguei).

Pasta com documentos e controles em [Google Drive](https://drive.google.com/drive/folders/0AH88MPeMrxDWUk9PVA)

---

## Contributors

- João Vitor paulino martins (paulino.joaovitor@yahoo.com.br)