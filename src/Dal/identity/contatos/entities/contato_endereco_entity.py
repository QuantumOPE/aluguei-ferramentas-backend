from peewee import *

from Dal._core.pewee_base_entity import PeeweeBaseEntity


class ContatoEnderecoEntity(PeeweeBaseEntity):
    class Meta:
        db_table = 'tb_contato_endereco'

    logradouro: CharField = CharField(unique=False, null=True)
    numero: CharField = CharField(unique=False, null=True)
    complemento: CharField = CharField(unique=False, null=True)
    bairro: CharField = CharField(unique=False, null=True)
    cep: CharField = CharField(unique=False, null=True)
    dtAtualizacao: DateTimeField = DateTimeField(unique=False, null=True)

    idContato: IntegerField = IntegerField(unique=False, null=True)
    idTipoContato: IntegerField = IntegerField(unique=False, null=True)
    idCidade: IntegerField = IntegerField(unique=False, null=True)
