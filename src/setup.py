from setuptools import setup

setup(
    name='Aluguei',
    version='1.0',
    packages=['Tests', 'Application', 'Application.InfraStructure', 'Application.InfraStructure.Util',
              'Application.DataAccessLayer', 'Application.DataAccessLayer._core',
              'Application.DataAccessLayer._core.Conections', 'Application.DataAccessLayer._core.Repository',
              'Application.ApplicationLayer', 'Application.ApplicationLayer.flaskr',
              'Application.ApplicationLayer.flaskr.Api', 'Application.ApplicationLayer.flaskr.Api.users',
              'Application.BusinessRulesLayer', 'Application.DataTransferObjectLayer',
              'Application.DataTransferObjectLayer._core', 'Application.DataTransferObjectLayer.users'],
    url='',
    license='GPL 3.0',
    author='joao vitor paulino martins',
    author_email='paulino.joaovitor@yahoo.com.br',
    description='Plataforma de aluguel de ferramentas'
)
