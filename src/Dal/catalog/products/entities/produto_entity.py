from peewee import *

from Dal._core.pewee_base_entity import PeeweeBaseEntity
from Dal.catalog.brands.entities.marca_entity import MarcaEntity


class ProdutoEntity(PeeweeBaseEntity):
    class Meta:
        db_table = 'tb_produto'

    idMarca: ForeignKeyField = ForeignKeyField(MarcaEntity, null=True, db_column="idMarca")

    nome: CharField = CharField(unique=False, null=True, max_length=100)
    caracteristicas: CharField = CharField(unique=False, null=True, max_length=8000)
    descricao: CharField = CharField(unique=False, null=True, max_length=8000)
    urlFotoPrincipal: CharField = CharField(unique=False, null=True, max_length=8000)
