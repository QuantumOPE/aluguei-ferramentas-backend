from datetime import datetime


class MarcaDto:
    id: int = None
    flagAtivo: bool = None
    dtCadastro: datetime = None

    nome: str = None
    descricao: str = None
    urlLogomarca: str = None

