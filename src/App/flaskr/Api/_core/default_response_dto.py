from typing import List

from Infra.Util.ObjectExtensions import ObjectExtensions


class DefaultResponseDto:
    def __init__(self, messages: [] = None, results: [] = None, arguments: [] = None):
        self.arguments: List[object] = arguments or list()
        self.messages: List[object] = messages or list()
        self.results: List[object] = results or list()

    def to_dictionary(self):
        return ObjectExtensions.to_full_dict(self)
