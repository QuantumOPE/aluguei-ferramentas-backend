from peewee import *

from Dal._core.pewee_base_entity import PeeweeBaseEntity
from Dal.catalog.categories.entities.departamento_entity import DepartamentoEntity


class CategoriaEntity(PeeweeBaseEntity):
    class Meta:
        db_table = 'tb_categoria'

    idDepartamento: ForeignKeyField = ForeignKeyField(DepartamentoEntity)

    descricao: CharField = CharField(unique=False, null=True)



