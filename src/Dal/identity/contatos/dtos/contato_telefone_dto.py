from datetime import datetime


class ContatoTelefoneDto:
    id: int = None
    flagAtivo: bool = None
    dtCadastro: datetime = None

    numeroTelefone: str = None
    idTipoTelefone: int = None
    dtAtualizacao: datetime = None
