from typing import Dict, List

from Dal.identity.contatos.entities.contato_telefone_entity import ContatoTelefoneEntity


class ContatoTelefoneDataAccess:
    def __init__(self):
        self.entity = ContatoTelefoneEntity()

    def migrate_table(self):
        self.entity.create_table()

    def query(self) -> ContatoTelefoneEntity:
        return self.entity

    @staticmethod
    def filtering(filters: {} = None) -> List:
        filters = filters or dict()

        filterExpression: List = [1 == 1]

        if 'flagAtivo' in filters and filters['flagAtivo'] != "":
            filterExpression.append(ContatoTelefoneEntity.flagAtivo == filters['flagAtivo'])

        if 'idTipoTelefone' in filters and filters['idTipoTelefone'] != "":
            filterExpression.append(ContatoTelefoneEntity.idTipoTelefone == filters['idTipoTelefone'])

        if 'numeroTelefone' in filters and filters['numeroTelefone'] != "":
            filterExpression.append(ContatoTelefoneEntity.numeroTelefone.contains(filters['numeroTelefone']))

        return filterExpression

    def create_telefone(self, telefone: Dict) -> bool:
        return self.query().insert_many([telefone]).execute() > 0

    def update_telefone(self, telefone: Dict, id_telefone: int) -> bool:
        query = self.query().update(**telefone).where(ContatoTelefoneEntity.id == id_telefone)
        return query.execute() > 0

    def list_all_telefone(self, filters: {} = None):
        query = self.query().select().where(*self.filtering(filters)).dicts()
        return list(query)

    def get_telefone(self, id_telefone: int):
        query = self.query().select().where(ContatoTelefoneEntity.id == id_telefone).dicts()
        count = query.count()
        if count > 0:
            return query.get()
        return None

    def delete_telefone(self, id_telefone: int) -> int:
        return self.query().delete().where(ContatoTelefoneEntity.id == id_telefone).execute() > 0
