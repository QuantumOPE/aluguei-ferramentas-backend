import collections
from datetime import datetime
from decimal import Decimal
from unicodedata import decimal


class ObjectExtensions(object):
    @staticmethod
    def to_full_dict(obj):
        if isinstance(obj, datetime):
            return str(obj)
        if isinstance(obj, Decimal):
            return float(obj)
        if isinstance(obj, str):
            return obj
        elif isinstance(obj, dict):
            return dict((key, ObjectExtensions.to_full_dict(val)) for key, val in obj.items())
        elif isinstance(obj, collections.Iterable):
            return [ObjectExtensions.to_full_dict(val) for val in obj]
        elif hasattr(obj, '__dict__'):
            return ObjectExtensions.to_full_dict(vars(obj))
        elif hasattr(obj, '__slots__'):
            return ObjectExtensions.to_full_dict(dict((name, getattr(obj, name)) for name in getattr(obj, '__slots__')))
        return obj

    @staticmethod
    def get_props(cls):
        return [prop for prop in cls.__dict__.keys() if prop[:1] != '_']

    @staticmethod
    def get_values(cls):
        return [prop for prop in cls.__dict__.values() if prop[:1] != '_']

    @staticmethod
    def get_value_from_prop(type_class, prop_name):
        return getattr(type_class, prop_name)

    @staticmethod
    def to_dictionary(type_class, remove_none: bool = False):
        public_fields = {key: value for key, value in type_class.__dict__.items() if key[:1] != '_'}

        if remove_none:
            public_fields = {key: value for key, value in public_fields.items() if value is not None}

        return public_fields

    @staticmethod
    def to_tuple(type_class):
        return tuple([value for key, value in ObjectExtensions.to_dictionary(type_class).items()])

    @staticmethod
    def from_dict(type_class, dict_values):
        for key, value in dict_values.items():
            if key in type_class.__dict__.keys():
                setattr(type_class, key, dict_values[key])

        return type_class

    @staticmethod
    def dict_2_Object(type_class, dict_values):
        top = type(type_class, (object,), dict_values)
        seqs = tuple, list, set, frozenset
        for i, j in dict_values.items():
            if isinstance(j, dict):
                setattr(top, i, ObjectExtensions.dict_2_Object(i, j))
            elif isinstance(j, seqs):
                setattr(top, i, type(j)(ObjectExtensions.dict_2_Object(i, sj) if isinstance(sj, dict) else sj for sj in j))
            else:
                setattr(top, i, j)
        return top

    @staticmethod
    def from_tuple(type_class, tuple_values):
        index = 0
        for key, value in type_class.__dict__.items():
            setattr(type_class, key, tuple_values[index])
            index += 1

        return type_class
