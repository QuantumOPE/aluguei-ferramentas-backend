from Dal.catalog.products.data_access.produto_categoria_data_access import ProdutoCategoriaDataAccess
from Dal.catalog.categories.data_access.sub_categoria_data_access import SubCategoriaDataAccess
from Dal.catalog.products.data_access.produto_data_access import ProdutoDataAccess
from Dal.catalog.products.entities.produto_entity import ProdutoEntity


class ProdutoFiller:
    def __init__(self):
        self.produto_dal = ProdutoDataAccess()
        self.produto_categoria_dal = ProdutoCategoriaDataAccess()
        self.produto_sub_categoria_dal = SubCategoriaDataAccess()

    def preencher_categorias(self, id_produto: int):
        produtos = self.produto_dal.query().select().where(ProdutoEntity.id == id_produto).get()

        categs = [categ for categ in (produtos.categorias.dicts())]

        return categs
