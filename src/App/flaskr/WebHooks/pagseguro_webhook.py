import json

from flask import Blueprint, request

from Bll.checkout_pag_seguro import PaymentPagSeguro
from Infra.Util.ObjectExtensions import ObjectExtensions

pagseguro_webhook: Blueprint = Blueprint(name=__name__,
                                         import_name=__name__,
                                         template_folder='templates')


@pagseguro_webhook.route('/webhook/v1/pagseguro/notification', methods=['POST'])
def notification_action():
    pague_seguro = PaymentPagSeguro()

    prequest = request.get_json()
    ps_noti = pague_seguro.notification_view(prequest)

    with open('notifications.json', 'r+') as file:
        all_notifications = file.read().replace('\n', '')

    if all_notifications == "":
        all_notifications = '{"notifications": []}'

    notifications_list = json.loads(str(all_notifications))
    notifications_list["notifications"].append(prequest)

    with open('notifications.json', 'w+') as file:
        file.write(json.dumps(notifications_list))

    return notifications_list, 201


@pagseguro_webhook.route('/webhook/v1/pagseguro/notification', methods=['GET'])
def notification_list_action():
    with open('notifications.json', 'r+') as file:
        notifications_list = file.read().replace('\n', '')

    return notifications_list, 200
