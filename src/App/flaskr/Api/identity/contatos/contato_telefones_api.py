from typing import Tuple, Any

from flask import request
from flask_restful import Resource


from App.flaskr.Api._core.default_response_dto import DefaultResponseDto
from Dal.identity.contatos.data_access.contato_telefone_data_access import ContatoTelefoneDataAccess
from Dal.identity.contatos.dtos.contato_telefone_dto import ContatoTelefoneDto
from Infra.Util.ObjectExtensions import ObjectExtensions


class TelefonesApi(Resource):
    uri: str = '/api/v1/identity/telphones'

    def __init__(self):
        self.telphones_dal = ContatoTelefoneDataAccess()
        self.args = request.args
        self.response: DefaultResponseDto = DefaultResponseDto()
        self.response.arguments = self.args

    def get(self, id_contact_telphone: int = 0):
        if id_contact_telphone > 0:
            return self.__get_single(id_contact_telphone)
        return self.__get_all()

    def __get_single(self, id_contact_telphone: int) -> Tuple[Any, int]:
        user = self.telphones_dal.get_telefone(id_contact_telphone)

        if user is not None:
            self.response.messages = ["registros encontrados com sucesso id: " + str(id_contact_telphone)]
            self.response.results = [user]

            return self.response.to_dictionary(), 200

        self.response.messages = ["nenhum registro encontrado com o id: " + str(id_contact_telphone)]
        return self.response.to_dictionary(), 404

    def __get_all(self) -> Tuple[Any, int]:
        allcompanies_list = self.telphones_dal.list_all_telefone(self.args)
        if len(allcompanies_list) > 0:
            self.response.messages = ["registros encontrado com sucesso"]
            self.response.results = allcompanies_list

            return self.response.to_dictionary(), 200

        self.response.messages = ["nenhum registro encontrado"]
        return self.response.to_dictionary(), 404

    def post(self) -> Tuple[Any, int]:
        if request.get_json() is None:
            self.response.messages.append("dados enviados não condizentes")
            return self.response.to_dictionary(), 403

        data_mapping = ObjectExtensions.from_dict(ContatoTelefoneDto, request.get_json())
        data_dict = ObjectExtensions.to_dictionary(data_mapping, True)

        flag_cadastrado = self.telphones_dal.create_telefone(data_dict)

        if flag_cadastrado:
            self.response.messages = ["registro cadastrado com sucesso"]
            return self.response.to_dictionary(), 201

        self.response.messages = ["algo errado ocorreu no processo de cadastro"]
        return self.response.to_dictionary(), 404

    def put(self, id_contact_telphone: int) -> Tuple[Any, int]:
        if request.get_json() is None:
            self.response.messages.append("dados enviados não condizentes")
            return self.response.to_dictionary(), 403

        company_data_mapping = ObjectExtensions.from_dict(ContatoTelefoneDto, request.get_json())
        company_data_dict = ObjectExtensions.to_dictionary(company_data_mapping, True)

        flag_atualizado = self.telphones_dal.update_telefone(company_data_dict, id_contact_telphone)

        if flag_atualizado:
            self.response.messages.append("registro atualizado com sucesso")
            return self.response.to_dictionary(), 201

        self.response.messages.append("Registro não encontrado")
        return self.response.to_dictionary(), 404

    def delete(self, id_contact_telphone: int) -> Tuple[Any, int]:
        flag_excluido = self.telphones_dal.delete_telefone(id_contact_telphone)

        if flag_excluido:
            self.response.messages.append("registro excluido com sucesso")
            return self.response.to_dictionary(), 201

        self.response.messages.append("registro não encontrado")
        return self.response.to_dictionary(), 404
