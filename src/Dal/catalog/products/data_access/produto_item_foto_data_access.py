from typing import Dict, List

from Dal.catalog.products.entities.produto_item_foto_entity import ProdutoItemFotoEntity


class ProdutoItemFotoDataAccess:
    def __init__(self):
        self.entity = ProdutoItemFotoEntity()

    def migrate_table(self):
        self.entity.create_table()

    def query(self) -> ProdutoItemFotoEntity:
        return self.entity

    @staticmethod
    def filtering(filters: {} = None) -> List:
        filters = filters or dict()

        filterExpression: List = [1 == 1]

        if 'idProdutoItem' in filters and filters['idProdutoItem'] != "":
            filterExpression.append(ProdutoItemFotoEntity.idProdutoItem == filters['idProdutoItem'])

        return filterExpression

    def create_product_item_foto(self, new_product_item_foto: Dict) -> bool:
        return self.query().insert_many([new_product_item_foto]).execute() > 0

    def update_product_item_foto(self, update_product_item_foto: Dict, id_product_item_foto: int) -> bool:
        query = self.query().update(**update_product_item_foto).where(ProdutoItemFotoEntity.id == id_product_item_foto)
        return query.execute() > 0

    def list_all_product_item_foto(self, filters: {} = None) -> List:
        query = self.query().select().where(*self.filtering(filters)).dicts()
        return list(query)

    def get_product_item_foto(self, id_product_item_foto: int) -> ProdutoItemFotoEntity:
        query = self.query().select().where(ProdutoItemFotoEntity.id == id_product_item_foto).dicts()
        count = query.count()
        if count > 0:
            return query.get()
        return None

    def delete_product_item_foto(self, id_product_item_foto: int) -> bool:
        return self.query().delete().where(ProdutoItemFotoEntity.id == id_product_item_foto).execute() > 0
