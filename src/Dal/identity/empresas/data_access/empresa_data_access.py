from typing import Dict, List

from Dal.identity.empresas.entities.empresa_entity import EmpresaEntity


class EmpresaDataAccess:
    def __init__(self):
        self.entity = EmpresaEntity()

    def migrate_table(self):
        self.entity.create_table()

    def query(self) -> EmpresaEntity:
        return self.entity

    @staticmethod
    def filtering(filters: {} = None) -> List:
        filters = filters or dict()

        filterExpression: List = [1 == 1]

        if 'flagAtivo' in filters and filters['flagAtivo'] != "":
            filterExpression.append(EmpresaEntity.flagAtivo == filters['flagAtivo'])

        if 'atividadeEmpresa' in filters and filters['atividadeEmpresa'] != "":
            filterExpression.append(EmpresaEntity.atividadeEmpresa.contains(filters['atividadeEmpresa']))

        if 'descricao' in filters and filters['descricao'] != "":
            filterExpression.append(EmpresaEntity.descricao.contains(filters['descricao']))

        if 'inscricaoEstadual' in filters and filters['inscricaoEstadual'] != "":
            filterExpression.append(EmpresaEntity.inscricaoEstadual.contains(filters['inscricaoEstadual']))

        if 'nomeFantasia' in filters and filters['nomeFantasia'] != "":
            filterExpression.append(EmpresaEntity.nomeFantasia.contains(filters['nomeFantasia']))

        if 'numeroCnpj' in filters and filters['numeroCnpj'] != "":
            filterExpression.append(EmpresaEntity.numeroCnpj.contains(filters['numeroCnpj']))

        if 'razaoSocial' in filters and filters['razaoSocial'] != "":
            filterExpression.append(EmpresaEntity.razaoSocial.contains(filters['razaoSocial']))

        return filterExpression

    def create_company(self, company: Dict) -> bool:
        return self.query().insert_many([company]).execute() > 0

    def update_company(self, company: Dict, id_company: int) -> bool:
        query = self.query().update(**company).where(EmpresaEntity.id == id_company)
        return query.execute() > 0

    def list_all_company(self, filters: {} = None):
        query = self.query().select().where(*self.filtering(filters)).dicts()
        return list(query)

    def get_company(self, id_company: int):
        query = self.query().select().where(EmpresaEntity.id == id_company).dicts()
        count = query.count()
        if count > 0:
            return query.get()
        return None

    def delete_company(self, id_company: int) -> int:
        return self.query().delete().where(EmpresaEntity.id == id_company).execute() > 0
