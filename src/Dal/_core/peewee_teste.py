from peewee import *

from Dal._core.pewee_base_entity import PeeweeBaseEntity


class AuthorEntity(PeeweeBaseEntity):
    name: CharField = CharField(unique=False, null=True)


class BookEntity(PeeweeBaseEntity):
    title: CharField = CharField(unique=False, null=True)
    description: CharField = CharField(unique=False, null=True)
    dateCreated: DateTimeField = DateTimeField(unique=False, null=True)

    id_author: ForeignKeyField = ForeignKeyField(AuthorEntity)


if __name__ == '__main__':

    try:
        AuthorEntity.create_table()
        BookEntity.create_table()
    except OperationalError:
        print("Algo inesperado aconteceu!")

    # create single
    author_1 = AuthorEntity.create(name='H. G. Wells')
    author_2 = AuthorEntity.create(name='Julio Verne')

    # insert many
    books = [{
        'title': 'A Máquina do Tempo',
        'id_author': author_1,
    }, {
        'title': 'Guerra dos Mundos',
        'id_author': author_1,
    }, {
        'title': 'Volta ao Mundo em 80 Dias',
        'id_author': author_2,
    }, {
        'title': 'Vinte Mil Leguas Submarinas',
        'id_author': author_1,
    }]

    BookEntity.insert_many(books).execute()

    # select with inner Join
    book = BookEntity.select()\
                     .where(BookEntity.title == "Volta ao Mundo em 80 Dias")\
                     .dicts()\
                     .get()

    print(book)

    # select with inner Join
    books = BookEntity.select() \
                      .join(AuthorEntity) \
                      .where(AuthorEntity.name == 'H. G. Wells')
    print(books.count())

    for book in books:
        print(book.title)

    # update
    new_author = AuthorEntity.get(AuthorEntity.name == 'Julio Verne')
    book: BookEntity = BookEntity.get(BookEntity.title == "Vinte Mil Leguas Submarinas")

    book.id_author = new_author
    book.save()

    # delete by instance
    book = BookEntity.get(BookEntity.title == "Guerra dos Mundos")
    book.delete_instance()
