from typing import Tuple, Any

from flask import request
from flask_restful import Resource

from App.flaskr.Api._core.default_response_dto import DefaultResponseDto
from Dal.identity.empresas.data_access.empresa_data_access import EmpresaDataAccess
from Dal.identity.empresas.dtos.empresa_dto import EmpresaDto
from Infra.Util.ObjectExtensions import ObjectExtensions


class EmpresasApi(Resource):
    uri: str = '/api/v1/identity/companies'

    def __init__(self):
        self.companies_dal = EmpresaDataAccess()
        self.args = request.args
        self.response: DefaultResponseDto = DefaultResponseDto()
        self.response.arguments = self.args

    def get(self, id_company: int = 0):
        if id_company > 0:
            return self.__get_single(id_company)
        return self.__get_all()

    def __get_single(self, id_company: int) -> Tuple[Any, int]:
        user = self.companies_dal.get_company(id_company)

        if user is not None:
            self.response.messages = ["registros encontrados com sucesso id: " + str(id_company)]
            self.response.results = [user]

            return self.response.to_dictionary(), 200

        self.response.messages = ["nenhum registro encontrado com o id: " + str(id_company)]
        return self.response.to_dictionary(), 404

    def __get_all(self) -> Tuple[Any, int]:
        allcompanies_list = self.companies_dal.list_all_company(self.args)
        if len(allcompanies_list) > 0:
            self.response.messages = ["registros encontrado com sucesso"]
            self.response.results = allcompanies_list

            return self.response.to_dictionary(), 200

        self.response.messages = ["nenhum registro encontrado"]
        return self.response.to_dictionary(), 404

    def post(self) -> Tuple[Any, int]:
        if request.get_json() is None:
            self.response.messages.append("dados enviados não condizentes")
            return self.response.to_dictionary(), 403

        company_data_mapping = ObjectExtensions.from_dict(EmpresaDto, request.get_json())
        company_data_dict = ObjectExtensions.to_dictionary(company_data_mapping, True)

        flag_cadastrado = self.companies_dal.create_company(company_data_dict)

        if flag_cadastrado:
            self.response.messages = ["registro cadastrado com sucesso"]
            return self.response.to_dictionary(), 201

        self.response.messages = ["algo errado ocorreu no processo de cadastro"]
        return self.response.to_dictionary(), 404

    def put(self, id_company: int) -> Tuple[Any, int]:
        if request.get_json() is None:
            self.response.messages.append("dados enviados não condizentes")
            return self.response.to_dictionary(), 403

        company_data_mapping = ObjectExtensions.from_dict(EmpresaDto, request.get_json())
        company_data_dict = ObjectExtensions.to_dictionary(company_data_mapping, True)

        flag_atualizado = self.companies_dal.update_company(company_data_dict, id_company)

        if flag_atualizado:
            self.response.messages.append("registro atualizado com sucesso")
            return self.response.to_dictionary(), 201

        self.response.messages.append("Registro não encontrado")
        return self.response.to_dictionary(), 404

    def delete(self, id_company: int) -> Tuple[Any, int]:
        flag_excluido = self.companies_dal.delete_company(id_company)

        if flag_excluido:
            self.response.messages.append("registro excluido com sucesso")
            return self.response.to_dictionary(), 201

        self.response.messages.append("registro não encontrado")
        return self.response.to_dictionary(), 404
