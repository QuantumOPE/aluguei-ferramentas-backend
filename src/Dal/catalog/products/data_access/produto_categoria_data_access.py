from typing import Dict, List

from Dal.catalog.products.entities.produto_categoria_entity import ProdutoCategoriaEntity


class ProdutoCategoriaDataAccess:
    def __init__(self):
        self.entity = ProdutoCategoriaEntity()

    def migrate_table(self):
        self.entity.create_table()

    def query(self) -> ProdutoCategoriaEntity:
        return self.entity

    @staticmethod
    def filtering(filters: {} = None) -> List:
        filters = filters or dict()

        filterExpression: List = [1 == 1]

        if 'idProduto' in filters and filters['idProduto'] != "":
            filterExpression.append(ProdutoCategoriaEntity.idProduto == filters['idProduto'])

        if 'idCategoria' in filters and filters['idCategoria'] != "":
            filterExpression.append(ProdutoCategoriaEntity.idCategoria == filters['idCategoria'])

        if 'idSubcategoria' in filters and filters['idSubcategoria'] != "":
            filterExpression.append(ProdutoCategoriaEntity.idSubcategoria == filters['idSubcategoria'])

        return filterExpression

    def create_product_category(self, new_product_category: Dict) -> bool:
        return self.query().insert_many([new_product_category]).execute() > 0

    def update_product_category(self, update_product_category: Dict, id_product_category: int) -> bool:
        query = self.query().update(**update_product_category).where(ProdutoCategoriaEntity.id == id_product_category)
        return query.execute() > 0

    def list_all_product_category(self, filters: {} = None):
        query = self.query().select().where(*self.filtering(filters)).dicts()
        return list(query)

    def get_product_category(self, id_product_category: int):
        query = self.query().select().where(ProdutoCategoriaEntity.id == id_product_category).dicts()
        count = query.count()
        if count > 0:
            return query.get()
        return None

    def delete_product_category(self, id_product_category: int) -> int:
        return self.query().delete().where(ProdutoCategoriaEntity.id == id_product_category).execute() > 0
