from datetime import datetime


class EmpresaDto:

    id: int = None
    flagAtivo: bool = None
    dtCadastro: datetime = None

    nomeFantasia: str = None
    razaoSocial: str = None
    numeroCnpj: str = None
    inscricaoEstadual: str = None
    inscricaoMunicipal: str = None
    tipoContribuinte: str = None
    atividadeEmpresa: str = None
    descricao: str = None

    # Foreign Keys
    idEmpresaMatriz: int = None
    idContatoEndereco: int = None
    idContatoTelefone: int = None
