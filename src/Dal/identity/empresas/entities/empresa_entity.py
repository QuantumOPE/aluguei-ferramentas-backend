from peewee import *

from Dal._core.pewee_base_entity import PeeweeBaseEntity


class EmpresaEntity(PeeweeBaseEntity):
    class Meta:
        db_table = 'tb_empresa'

    nomeFantasia: CharField = CharField(unique=False, null=True)
    razaoSocial: CharField = CharField(unique=False, null=True)
    numeroCnpj: CharField = CharField(unique=False, null=True)
    inscricaoEstadual: CharField = CharField(unique=False, null=True)
    inscricaoMunicipal: CharField = CharField(unique=False, null=True)
    tipoContribuinte: CharField = CharField(unique=False, null=True)
    atividadeEmpresa: CharField = CharField(unique=False, null=True)
    descricao: CharField = CharField(unique=False, null=True)

    # Foreign Keys
    idEmpresaMatriz: IntegerField = IntegerField(unique=False, null=True)
    idContatoEndereco: IntegerField = IntegerField(unique=False, null=True)
    idContatoTelefone: IntegerField = IntegerField(unique=False, null=True)

    dtCadastro: DateTimeField = DateTimeField(unique=False, null=True)

