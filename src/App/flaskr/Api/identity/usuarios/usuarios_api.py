import json
from typing import Tuple, Any

import requests
from flask import request
from flask_restful import Resource

from App.flaskr.Api._core.default_response_dto import DefaultResponseDto
from Dal.identity.usuarios.data_access.usuario_data_access import UsuarioDataAccess
from Dal.identity.usuarios.dtos.usuario_dto import UsuarioDto
from Infra.Util.ObjectExtensions import ObjectExtensions


class UsuariosApi(Resource):
    uri: str = '/api/v1/identity/users'

    def __init__(self):
        self.users_dal = UsuarioDataAccess()
        self.args = request.args
        self.response: DefaultResponseDto = DefaultResponseDto()
        self.response.arguments = self.args

    def get(self, id_user: int = 0):
        if id_user > 0:
            return self.__get_single(id_user)
        return self.__get_all()

    def __get_single(self, id_user: int) -> Tuple[Any, int]:
        user = self.users_dal.get_usuario(id_user)

        if user is not None:
            self.response.messages = ["registros encontrados com sucesso id: " + str(id_user)]
            self.response.results = [user]

            return self.response.to_dictionary(), 200

        self.response.messages = ["nenhum registro encontrado com o id: " + str(id_user)]
        return self.response.to_dictionary(), 404

    def __get_all(self) -> Tuple[Any, int]:
        all_users_list = self.users_dal.list_all_usuario(self.args)
        if len(all_users_list) > 0:
            self.response.messages = ["registros encontrado com sucesso"]
            self.response.results = all_users_list

            return self.response.to_dictionary(), 200

        self.response.messages = ["nenhum registro encontrado"]
        return self.response.to_dictionary(), 404

    def post(self) -> Tuple[Any, int]:
        if request.get_json() is None:
            self.response.messages.append("dados enviados não condizentes")
            return self.response.to_dictionary(), 403

        user_data_mapping = ObjectExtensions.from_dict(UsuarioDto, request.get_json())
        user_data_dict = ObjectExtensions.to_dictionary(user_data_mapping, True)

        flagRepetido, messageRepetido = self.users_dal.verificar_duplicidade(user_data_dict)

        if flagRepetido:
            self.response.messages = [messageRepetido]
            return self.response.to_dictionary(), 403

        flag_cadastrado = self.users_dal.create_usuario(user_data_dict)

        if flag_cadastrado:
            self.response.messages = ["registro cadastrado com sucesso"]

            url = 'https://tt2x1pnm69.execute-api.us-east-1.amazonaws.com/email-service'
            headers = {'content-type': 'application/json'}
            payloadEmail = {
                "to": user_data_dict['email'],
                "name": user_data_dict['nome'],
                "content": 0
            }
            sendMail = requests.post(url, data=json.dumps(payloadEmail), headers=headers)
            print(sendMail.text)

            return self.response.to_dictionary(), 201

        self.response.messages = ["algo errado ocorreu no processo de cadastro"]
        return self.response.to_dictionary(), 404

    def put(self, id_user: int) -> Tuple[Any, int]:
        if request.get_json() is None:
            self.response.messages.append("dados enviados não condizentes")
            return self.response.to_dictionary(), 403

        user_data_mapping = ObjectExtensions.from_dict(UsuarioDto, request.get_json())
        user_data_dict = ObjectExtensions.to_dictionary(user_data_mapping, True)

        flag_atualizado = self.users_dal.update_usuario(user_data_dict, id_user)

        if flag_atualizado:
            self.response.messages.append("registro atualizado com sucesso")
            return self.response.to_dictionary(), 201

        self.response.messages.append("Registro não encontrado")
        return self.response.to_dictionary(), 404

    def delete(self, id_user: int) -> Tuple[Any, int]:
        flag_excluido = self.users_dal.delete_usuario(id_user)

        if flag_excluido:
            self.response.messages.append("registro excluido com sucesso")
            return self.response.to_dictionary(), 201

        self.response.messages.append("registro não encontrado")
        return self.response.to_dictionary(), 404
