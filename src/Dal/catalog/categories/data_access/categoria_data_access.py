from typing import Dict, List

from Dal.catalog.categories.entities.categoria_entity import CategoriaEntity


class CategoriaDataAccess:
    def __init__(self):
        self.entity = CategoriaEntity()

    def migrate_table(self):
        self.entity.create_table()

    def query(self) -> CategoriaEntity:
        return self.entity

    @staticmethod
    def filtering(filters: {} = None) -> List:
        filters = filters or dict()

        filterExpression: List = [1 == 1]

        if 'descricao' in filters and filters['descricao'] != "":
            filterExpression.append(CategoriaEntity.descricao == filters['descricao'])

        return filterExpression

    def create_categoria(self, new_categoria: Dict) -> bool:
        return self.query().insert_many([new_categoria]).execute() > 0

    def update_categoria(self, update_categoria: Dict, id_categoria: int) -> bool:
        query = self.query().update(**update_categoria).where(CategoriaEntity.id == id_categoria)
        return query.execute() > 0

    def list_all_categoria(self, filters: {} = None) -> List:
        query = self.query().select().where(*self.filtering(filters)).dicts()
        return list(query)

    def get_categoria(self, id_categoria: int) -> CategoriaEntity:
        query = self.query().select().where(CategoriaEntity.id == id_categoria).dicts()
        count = query.count()
        if count > 0:
            return query.get()
        return None

    def delete_categoria(self, id_categoria: int) -> bool:
        return self.query().delete().where(CategoriaEntity.id == id_categoria).execute() > 0
