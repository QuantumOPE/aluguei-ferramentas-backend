from datetime import datetime


class ContatoEnderecoDto:
    id: int = None
    flagAtivo: bool = None
    dtCadastro: datetime = None

    logradouro: str = None
    numero: str = None
    complemento: str = None
    bairro: str = None
    cep: str = None

    idContato: int = None
    idTipoContato: int = None
    idCidade: int = None

    dtAtualizacao: datetime = None
