from peewee import *

from Dal._core.pewee_base_entity import PeeweeBaseEntity
from Dal.catalog.products.entities.produto_item_entity import ProdutoItemEntity


class ProdutoItemFotoEntity(PeeweeBaseEntity):
    class Meta:
        db_table = 'tb_produto_item_foto'

    idProdutoItem: ForeignKeyField = ForeignKeyField(ProdutoItemEntity, null=True, db_column="idProdutoItem")
    url: CharField = CharField(unique=False, null=True)
