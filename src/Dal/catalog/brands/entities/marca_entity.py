from peewee import *

from Dal._core.pewee_base_entity import PeeweeBaseEntity


class MarcaEntity(PeeweeBaseEntity):
    class Meta:
        db_table = 'tb_marca'

    nome: CharField = CharField(unique=False, null=True)
    descricao: CharField = CharField(unique=False, null=True)

    urlLogomarca: CharField = CharField(unique=False, null=True)

