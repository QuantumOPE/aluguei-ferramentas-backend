from App.config.environment import Environment
from Dal._core.data_context_singleton import DataContextSingleton
from App.startup import RanditApp

if __name__ == "__main__":
    app = RanditApp()

    environment = Environment(flag_production=False)

    app.app_start(environment)
