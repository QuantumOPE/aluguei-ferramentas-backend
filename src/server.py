from datetime import datetime
import os, time

while 1:
    print("/usr/bin/pkill -f main.py")
    os.system("/usr/bin/pkill -9 -f main.py")
    print("starting again")
    os.system("nohup python3 main.py &") 

    today = datetime.now().strftime("%d/%m/%Y %H:%M:%S") 
    os.system(f"echo 'restart at' {today} >> ../server/reboot.log")

    time.sleep(600)
    print("Restarting...")
