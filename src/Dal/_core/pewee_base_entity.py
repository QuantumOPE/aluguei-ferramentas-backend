import pymysql as pymysql
from peewee import *
from Dal._core.data_context_singleton import *


class PeeweeBaseEntity(Model):
    class Meta:
        database = DataContextSingleton.getInstance().database

    id: AutoField = AutoField(unique=True, null=False, primary_key=True)

    flagAtivo: BooleanField = BooleanField(unique=False, null=False, default=1)

    dtCadastro: DateTimeField = DateTimeField(unique=False, null=True)
