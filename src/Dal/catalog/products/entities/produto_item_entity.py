from peewee import *

from Dal._core.pewee_base_entity import PeeweeBaseEntity
from Dal.catalog.brands.entities.marca_entity import MarcaEntity
from Dal.catalog.products.entities.produto_entity import ProdutoEntity


class ProdutoItemEntity(PeeweeBaseEntity):
    class Meta:
        db_table = 'tb_produto_item'

    idProduto: ForeignKeyField = ForeignKeyField(ProdutoEntity, null=True, db_column="idProduto")

    idEmpresa: IntegerField = IntegerField(unique=False, null=True, db_column="idEmpresa")

    nome: CharField = CharField(unique=False, null=True)
    descricao: CharField = CharField(unique=False, null=True)
    numeroSerie: CharField = CharField(unique=False, null=True)
    precoHora: DecimalField = DecimalField(unique=False, max_digits=8, decimal_places=2)
    flagDisponivel: BooleanField = BooleanField(unique=False, null=False, default=1)
    dtFabricacao: DateTimeField = DateTimeField(unique=False, null=True)
