from flask import Flask, request, make_response
from flask_restful import Api

from App.config.environment import Environment
from App.flaskr.Api.catalog.products.produtos_itens_api import ProdutosItemApi
from App.flaskr.Api.identity.contatos.contato_endereco_api import ContatoEnderecoApi
from App.flaskr.Api.identity.contatos.contato_telefones_api import TelefonesApi
from App.flaskr.Api.identity.empresas.empresas_api import EmpresasApi
from App.flaskr.Api.identity.usuarios.usuarios_api import UsuariosApi
from App.flaskr.Api.catalog.department.departamento_api import departamentos_api
from App.flaskr.Api.catalog.products.produtos_api import ProdutosApi
from App.flaskr.Api.orders.checkout_api import CheckoutApi
from App.flaskr.WebHooks.pagseguro_webhook import pagseguro_webhook
from Dal._core.data_context_singleton import DataContextSingleton


class RanditApp:

    @staticmethod
    def app_start(environment: Environment):
        app = Flask(__name__)
        api = Api(app)

        app.register_blueprint(departamentos_api)
        app.register_blueprint(pagseguro_webhook)

        api.add_resource(ContatoEnderecoApi, ContatoEnderecoApi.uri,  ContatoEnderecoApi.uri + '/<int:id_contact_address>')
        api.add_resource(ProdutosApi, ProdutosApi.uri,  ProdutosApi.uri + '/<int:id_product>')
        api.add_resource(ProdutosItemApi, ProdutosItemApi.uri,  ProdutosItemApi.uri + '/<int:id_item>')
        api.add_resource(UsuariosApi, UsuariosApi.uri,  UsuariosApi.uri + '/<int:id_user>')
        api.add_resource(EmpresasApi, EmpresasApi.uri,  EmpresasApi.uri + '/<int:id_company>')
        api.add_resource(TelefonesApi, TelefonesApi.uri,  TelefonesApi.uri + '/<int:id_contact_telphone>')
        api.add_resource(CheckoutApi, CheckoutApi.uri,  CheckoutApi.uri + '/')

        server_conf = environment.get_server_host_conf()

        @app.before_request
        def before_request():
            if request.method == 'OPTIONS':
                response = make_response()
                return response

            DataContextSingleton.getInstance().database.connect()
            print("Open connect")

        @app.after_request
        def after_request(response):
            DataContextSingleton.getInstance().database.close()
            print("Close connect")

            response.headers.add('Access-Control-Allow-Origin', '*')
            response.headers.add('Access-Control-Allow-Headers', 'Content-Type, Authorization, '
                                                                 'Access-Control-Allow-Origin')

            response.headers.add('Access-Control-Allow-Methods', 'GET, PUT, POST, DELETE, OPTIONS')

            return response

        app.run(
            host=server_conf["host"],
            port=server_conf["port"],
            debug=server_conf["debug"]
        )
