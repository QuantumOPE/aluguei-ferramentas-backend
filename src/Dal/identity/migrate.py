from Dal.catalog.brands.data_access.marca_data_access import MarcaDataAccess
from Dal.catalog.categories.data_access.categoria_data_access import CategoriaDataAccess
from Dal.catalog.categories.data_access.sub_categoria_data_access import SubCategoriaDataAccess
from Dal.catalog.categories.data_access.departamento_data_access import DepartamentoDataAccess
from Dal.catalog.products.data_access.produto_data_access import ProdutoDataAccess
from Dal.catalog.products.data_access.produto_item_data_access import ProdutoItemDataAccess
from Dal.catalog.products.data_access.produto_item_foto_data_access import ProdutoItemFotoDataAccess

dalDepartamento = DepartamentoDataAccess()
dalCategoria = CategoriaDataAccess()
dalSubCategoria = SubCategoriaDataAccess()
dalMarcas = MarcaDataAccess()
dalProduto = ProdutoDataAccess()
dalProdutoItem = ProdutoItemDataAccess()
dalProdutoFoto = ProdutoItemFotoDataAccess()

dalDepartamento.migrate_table()
dalCategoria.migrate_table()
dalSubCategoria.migrate_table()
dalMarcas.migrate_table()
dalProduto.migrate_table()
dalProdutoItem.migrate_table()
dalProdutoFoto.migrate_table()
