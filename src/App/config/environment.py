from os.path import dirname, abspath, join


class Environment:
    aws_server_url = 'http://ec2-18-230-71-213.sa-east-1.compute.amazonaws.com:5080/'

    def __init__(self, flag_production: bool):
        self._flag_production = flag_production
        self._SERVER_HOST_PROD = {
            'host': '0.0.0.0',
            'port': 5080,
            'debug': True
        }
        self._DB_CONN_DEV = {
            'server': 'randit-database.c4zy5pdhcexn.sa-east-1.rds.amazonaws.com',
            'port': 3306,
            'user': 'admin',
            'password': 'randit123',
            'database': 'db_randit_dev'
        }
        self._DB_CONN_PROD = {
            'server': 'randit-database.c4zy5pdhcexn.sa-east-1.rds.amazonaws.com',
            'port': 3306,
            'user': 'admin',
            'password': 'randit123',
            'database': 'db_randit_prod'
        }
        self._APPLICATION_DIR = dirname(dirname(abspath(__file__)))
        self._PROJECT_DIR = dirname(dirname(dirname(dirname(__file__))))

    def get_full_path(self, *path):
        return self._PROJECT_DIR + join(self._PROJECT_DIR, *path)

    def get_project_path(self):
        return self._PROJECT_DIR

    def get_application_path(self):
        return self._APPLICATION_DIR

    def get_temp_files_path(self):
        return self._PROJECT_DIR + "/Shared/TempFiles/"

    def get_uploads_path(self):
        return self._PROJECT_DIR + "/Shared/Uploads/"

    def get_server_host_conf(self):
        return self._SERVER_HOST_PROD if self._flag_production is True else self._SERVER_HOST_PROD

    def get_database_conf(self):
        return self._DB_CONN_PROD if self._flag_production is True else self._DB_CONN_DEV
