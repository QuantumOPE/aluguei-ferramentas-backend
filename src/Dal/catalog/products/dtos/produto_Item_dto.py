from datetime import datetime


class ProdutoItemDto:
    id: int = None
    flagAtivo: bool = None
    dtCadastro: datetime = None

    idProduto: int = None
    idEmpresa: int = None

    nome: str = None
    descricao: str = None
    numeroSerie: str = None
    precoHora: str = None
    flagDisponivel: bool = None
    dtFabricacao: datetime = None
