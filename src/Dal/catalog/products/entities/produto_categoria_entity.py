from peewee import *

from Dal._core.pewee_base_entity import PeeweeBaseEntity
from Dal.catalog.categories.entities.categoria_entity import CategoriaEntity
from Dal.catalog.categories.entities.sub_categoria_entity import SubCategoriaEntity
from Dal.catalog.products.entities.produto_entity import ProdutoEntity


class ProdutoCategoriaEntity(PeeweeBaseEntity):
    class Meta:
        db_table = 'tb_produto_categoria'

    idProduto: ForeignKeyField = ForeignKeyField(ProdutoEntity, null=True, db_column="idProduto", backref='categorias')
    idCategoria: ForeignKeyField = ForeignKeyField(CategoriaEntity, null=True, db_column="idCategoria", backref='categoria')
    idSubcategoria: ForeignKeyField = ForeignKeyField(SubCategoriaEntity, null=True, db_column="idSubcategoria", backref='subCategoria')
