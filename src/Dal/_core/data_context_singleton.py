from typing import Dict

from peewee import MySQLDatabase

from App.config.environment import Environment

environment = Environment(flag_production=False)


class DataContextSingleton:
    __instance = None
    __conf = environment.get_database_conf()

    @staticmethod
    def getInstance(conf: Dict = None):
        conf = conf or DataContextSingleton.__conf
        if DataContextSingleton.__instance is None:
            DataContextSingleton(conf=conf)
        return DataContextSingleton.__instance

    def __init__(self, conf: Dict = None):
        if DataContextSingleton.__instance is not None:
            pass
        else:
            self.database = MySQLDatabase(database=conf['database'],
                                          user=conf['user'],
                                          password=conf['password'],
                                          host=conf['server'],
                                          port=conf['port'])

            print(self.database)

        DataContextSingleton.__instance = self
