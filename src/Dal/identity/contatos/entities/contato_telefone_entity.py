from peewee import *

from Dal._core.pewee_base_entity import PeeweeBaseEntity


class ContatoTelefoneEntity(PeeweeBaseEntity):
    class Meta:
        db_table = 'tb_contato_telefone'

    numeroTelefone: CharField = CharField(unique=False, null=True)

    idTipoTelefone: IntegerField = IntegerField(unique=False, null=True)

    dtAtualizacao: DateTimeField = DateTimeField(unique=False, null=True)
