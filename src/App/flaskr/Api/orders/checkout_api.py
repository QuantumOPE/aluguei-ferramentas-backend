from typing import Tuple, Any

from flask import request
from flask_restful import Resource

from App.flaskr.Api._core.default_response_dto import DefaultResponseDto
from Bll.checkout_pag_seguro import PaymentPagSeguro
from Dal.Orders.checkout.agreggates.checkout_aggregate import CheckoutAggregate
from Infra.Util.ObjectExtensions import ObjectExtensions


class CheckoutApi(Resource):
    uri: str = '/api/v1/orders/checkout'

    def __init__(self):
        self.payment_bll = PaymentPagSeguro()
        self.args = request.args
        self.response: DefaultResponseDto = DefaultResponseDto()
        self.response.arguments = self.args

    def post(self) -> Tuple[Any, int]:
        if request.get_json() is None:
            self.response.messages.append("dados enviados não condizentes")
            return self.response.to_dictionary(), 403

        order_data_json = request.get_json()

        ps_reponse = self.payment_bll.checkout(order_data_json)

        self.response.messages = ["Registro de compra efetuada"]
        ps_reponse.xml = ps_reponse.xml.decode("utf-8")
        self.response.results = ps_reponse

        return self.response.to_dictionary(), 201
