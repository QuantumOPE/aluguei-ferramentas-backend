from datetime import datetime


class ProdutoCategoriaDto:
    id: int = None
    flagAtivo: bool = None
    dtCadastro: datetime = None

    idProduto: int = None
    idCategoria: int = None
    idSubcategoria: int = None

