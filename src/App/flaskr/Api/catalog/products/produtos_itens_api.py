from typing import Tuple, Any

from flask import request
from flask_restful import Resource

from App.flaskr.Api._core.default_response_dto import DefaultResponseDto
from Dal.catalog.products.data_access.produto_item_data_access import ProdutoItemDataAccess
from Dal.catalog.products.dtos.produto_Item_dto import ProdutoItemDto
from Dal.catalog.products.dtos.produto_dto import ProdutoDto
from Infra.Util.ObjectExtensions import ObjectExtensions


class ProdutosItemApi(Resource):
    uri: str = '/api/v1/catalog/itens'

    def __init__(self):
        self.products_dal = ProdutoItemDataAccess()
        self.args = request.args
        self.response: DefaultResponseDto = DefaultResponseDto()
        self.response.arguments = self.args

    def get(self, id_item: int = 0):
        if id_item > 0:
            return self.__get_single(id_item)
        return self.__get_all()

    def __get_single(self, id_item: int) -> Tuple[Any, int]:
        product = self.products_dal.get_product_item(id_item)

        if product is not None:
            self.response.messages = ["registros encontrados com sucesso id: " + str(id_item)]
            self.response.results = [product]

            return self.response.to_dictionary(), 200

        self.response.messages = ["nenhum registro encontrado com o id: " + str(id_item)]
        return self.response.to_dictionary(), 404

    def __get_all(self) -> Tuple[Any, int]:
        all_productss_list = self.products_dal.list_all_product_item(self.args)
        if len(all_productss_list) > 0:
            self.response.messages = ["registros encontrado com sucesso"]
            self.response.results = all_productss_list

            return self.response.to_dictionary(), 200

        self.response.messages = ["nenhum registro encontrado"]
        return self.response.to_dictionary(), 404

    def post(self) -> Tuple[Any, int]:
        if request.get_json() is None:
            self.response.messages.append("dados enviados não condizentes")
            return self.response.to_dictionary(), 403

        user_data_mapping = ObjectExtensions.from_dict(ProdutoDto, request.get_json())
        user_data = ObjectExtensions.to_dictionary(user_data_mapping, True)

        flag_cadastrado = self.products_dal.create_product_item(request.get_json())

        if flag_cadastrado:
            self.response.messages = ["registro cadastrado com sucesso"]
            return self.response.to_dictionary(), 201

        self.response.messages = ["algo errado ocorreu no processo de cadastro"]
        return self.response.to_dictionary(), 404

    def put(self, id_item: int) -> Tuple[Any, int]:
        if request.get_json() is None:
            self.response.messages.append("dados enviados não condizentes")
            return self.response.to_dictionary(), 403

        product_data_mapping = ObjectExtensions.from_dict(ProdutoItemDto, request.get_json())
        product_data_dict = ObjectExtensions.to_dictionary(product_data_mapping, True)

        flag_atualizado = self.products_dal.update_product_item(product_data_dict, id_item)

        if flag_atualizado:
            self.response.messages.append("registro atualizado com sucesso")
            return self.response.to_dictionary(), 201

        self.response.messages.append("Registro não encontrado")
        return self.response.to_dictionary(), 404

    def delete(self, id_item: int) -> Tuple[Any, int]:
        flag_excluido = self.products_dal.delete_product_item(id_item)

        if flag_excluido:
            self.response.messages.append("registro excluido com sucesso")
            return self.response.to_dictionary(), 201

        self.response.messages.append("registro não encontrado")
        return self.response.to_dictionary(), 404
