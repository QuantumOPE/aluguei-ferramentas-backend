from typing import List

from Dal.Orders.checkout.dtos.order_item_dto import OrderItemDto
from Dal.Orders.checkout.dtos.order_sender_dto import OrderSenderDto
from Dal.Orders.checkout.dtos.order_shipping_dto import OrderShippingDto


class CheckoutAggregate:
    sender: OrderSenderDto = OrderSenderDto()
    shipping: OrderShippingDto = OrderShippingDto()
    items: List[OrderItemDto] = []
