from flask import Blueprint, jsonify, request
from Dal.catalog.categories.data_access.departamento_data_access import DepartamentoDataAccess

departamentos_api: Blueprint = Blueprint(name=__name__,
                                         import_name=__name__,
                                         template_folder='templates')


@departamentos_api.route('/departamentos/migrate', methods=['GET'])
def migrate_departamentos_action():
    departamentos_dal = DepartamentoDataAccess()
    departamentos_dal.migrate_table()

    return ""


@departamentos_api.route('/departamentos', methods=['GET'])
def list_productss_action():
    departamentos_dal = DepartamentoDataAccess()

    all_productss_list = departamentos_dal.list_all_departamento()

    return jsonify(all_productss_list)


@departamentos_api.route('/departamentos/<int:id>', methods=['GET'])
def find_departamentos_action(id: int) -> str:
    departamentos_dal = DepartamentoDataAccess()

    user = departamentos_dal.get_departamento(id)

    return jsonify(user)


@departamentos_api.route('/departamentos', methods=['POST'])
def new_departamentos_action():
    departamentos_dal = DepartamentoDataAccess()

    if request.get_json() is None:
        return "Erro de cadastro de usuarios"

    flag_cadastrado = departamentos_dal.create_departamento(request.get_json())

    return jsonify(flag_cadastrado)


@departamentos_api.route('/departamentos/<int:id>', methods=['DELETE'])
def remove_departamentos_action(id):
    departamentos_dal = DepartamentoDataAccess()
    flag_excluido = departamentos_dal.delete_departamento(id)

    return jsonify(flag_excluido)


@departamentos_api.route('/departamentos/<int:id>', methods=['PUT'])
def update_departamentos_action(id):
    departamentos_dal = DepartamentoDataAccess()
    user_data = request.get_json()

    flag_atualizado = departamentos_dal.update_departamento(user_data, id)

    return jsonify(flag_atualizado)
