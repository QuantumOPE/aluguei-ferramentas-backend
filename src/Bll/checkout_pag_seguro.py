from typing import List

from pagseguro import PagSeguro

from App.config.environment import Environment
from Dal.Orders.checkout.agreggates.checkout_aggregate import CheckoutAggregate


class PaymentPagSeguro:
    def __init__(self):
        self.__pg_session = PagSeguro(
            email="paulino.joaovitor@yahoo.com.br",
            token="51F0995D878840DF970E9D3550C7011A",
            config={
                'sandbox': True
            }
        )

    def checkout(self, order: dict):
        self.__pg_session.reference_prefix = "RENT"
        self.__pg_session.sender = order["sender"]
        self.__pg_session.shipping = order["shipping"]
        self.__pg_session.items = order["items"]
        self.__pg_session.notification_url = Environment.aws_server_url + "/webhook/v1/pagseguro/notification"

        pag_seguro_response = self.__pg_session.checkout()

        return pag_seguro_response

    def notification_view(self, ps_request):
        notification_code = ps_request.POST['notificationCode']
        notification_view = self.__pg_session.check_notification(notification_code)

        return notification_view
