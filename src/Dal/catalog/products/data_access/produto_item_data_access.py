from typing import Dict, List

from Dal.catalog.products.entities.produto_item_entity import ProdutoItemEntity


class ProdutoItemDataAccess:
    def __init__(self):
        self.entity = ProdutoItemEntity()

    def migrate_table(self):
        self.entity.create_table()

    def query(self) -> ProdutoItemEntity:
        return self.entity

    @staticmethod
    def filtering(filters: {} = None) -> List:
        filters = filters or dict()

        filterExpression: List = [1 == 1]

        if 'nome' in filters and filters['nome'] != "":
            filterExpression.append(ProdutoItemEntity.nome == filters['nome'])

        if 'descricao' in filters and filters['descricao'] != "":
            filterExpression.append(ProdutoItemEntity.descricao == filters['descricao'])

        if 'idProduto' in filters and filters['idProduto'] != "":
            filterExpression.append(ProdutoItemEntity.idProduto == filters['idProduto'])

        '''if 'idMarca' in filters and filters['idMarca'] != "":
            filterExpression.append(ProdutoItemEntity.idMarca == filters['idMarca'])
        '''

        if 'idEmpresa' in filters and filters['idEmpresa'] != "":
            filterExpression.append(ProdutoItemEntity.idEmpresa == filters['idEmpresa'])

        if 'numeroSerie' in filters and filters['numeroSerie'] != "":
            filterExpression.append(ProdutoItemEntity.numeroSerie.contains(filters['numeroSerie']))

        if 'precoHora' in filters and filters['precoHora'] != "":
            filterExpression.append(ProdutoItemEntity.precoHora == filters['precoHora'])

        if 'flagDisponivel' in filters and filters['flagDisponivel'] != "":
            filterExpression.append(ProdutoItemEntity.flagDisponivel == filters['flagDisponivel'])

        if 'dtFabricacao' in filters and filters['dtFabricacao'] != "":
            filterExpression.append(ProdutoItemEntity.dtFabricacao == filters['dtFabricacao'])

        return filterExpression

    def create_product_item(self, new_product_item: Dict) -> bool:
        return self.query().insert_many([new_product_item]).execute() > 0

    def update_product_item(self, update_product_item: Dict, id_product_item: int) -> bool:
        query = self.query().update(**update_product_item).where(ProdutoItemEntity.id == id_product_item)
        return query.execute() > 0

    def list_all_product_item(self, filters: {} = None) -> List:
        query = self.query().select().where(*self.filtering(filters)).dicts()
        return list(query)

    def get_product_item(self, id_product_item: int) -> ProdutoItemEntity:
        query = self.query().select().where(ProdutoItemEntity.id == id_product_item).dicts()
        count = query.count()
        if count > 0:
            return query.get()
        return None

    def delete_product_item(self, id_product_item: int) -> bool:
        return self.query().delete().where(ProdutoItemEntity.id == id_product_item).execute() > 0
