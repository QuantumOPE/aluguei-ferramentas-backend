from peewee import *

from Dal._core.pewee_base_entity import PeeweeBaseEntity


class UsuarioEntity(PeeweeBaseEntity):
    class Meta:
        db_table = 'tb_usuario'

    nome: CharField = CharField(unique=False, null=True)
    sobreNome: CharField = CharField(unique=False, null=True)
    numeroCpf: CharField = CharField(unique=False, null=True)
    email: CharField = CharField(unique=False, null=True)
    nomeUsuario: CharField = CharField(unique=False, null=True)
    senhaUsuario: CharField = CharField(unique=False, null=True)

    idUsuarioPerfilAcesso: IntegerField = IntegerField(unique=False, null=True)
    idContatoTelefone: IntegerField = IntegerField(unique=False, null=True)

    dtCadastro: DateTimeField = DateTimeField(unique=False, null=True)
