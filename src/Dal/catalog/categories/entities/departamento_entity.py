from peewee import *

from Dal._core.pewee_base_entity import PeeweeBaseEntity


class DepartamentoEntity(PeeweeBaseEntity):
    class Meta:
        db_table = 'tb_departamento'

    nome: CharField = CharField(unique=False, null=True)
    descricao: CharField = CharField(unique=False, null=True)



