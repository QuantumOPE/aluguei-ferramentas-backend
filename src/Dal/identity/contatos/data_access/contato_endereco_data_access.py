from typing import Dict, List

from Dal.identity.contatos.entities.contato_endereco_entity import ContatoEnderecoEntity


class ContatoEnderecoDataAccess:
    def __init__(self):
        self.entity = ContatoEnderecoEntity()

    def migrate_table(self):
        self.entity.create_table()

    def query(self) -> ContatoEnderecoEntity:
        return self.entity

    @staticmethod
    def filtering(filters: {} = None) -> List:
        filters = filters or dict()

        filterExpression: List = [1 == 1]

        if 'bairro' in filters and filters['bairro'] != "":
            filterExpression.append(ContatoEnderecoEntity.bairro.contains(filters['bairro']))

        if 'cep' in filters and filters['cep'] != "":
            filterExpression.append(ContatoEnderecoEntity.cep.contains(filters['cep']))

        return filterExpression

    def create_endereco(self, endereco: Dict) -> bool:
        return self.query().insert_many([endereco]).execute() > 0

    def update_endereco(self, endereco: Dict, id_endereco: int) -> bool:
        query = self.query().update(**endereco).where(ContatoEnderecoEntity.id == id_endereco)
        return query.execute() > 0

    def list_all_endereco(self, filters: {} = None):
        query = self.query().select().where(*self.filtering(filters)).dicts()
        return list(query)

    def get_endereco(self, id_endereco: int):
        query = self.query().select().where(ContatoEnderecoEntity.id == id_endereco).dicts()
        count = query.count()
        if count > 0:
            return query.get()
        return None

    def delete_endereco(self, id_endereco: int) -> int:
        return self.query().delete().where(ContatoEnderecoEntity.id == id_endereco).execute() > 0
