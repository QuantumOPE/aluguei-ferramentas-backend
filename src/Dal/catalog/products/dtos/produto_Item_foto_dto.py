from datetime import datetime


class ProdutoItemDto:
    id: int = None
    flagAtivo: bool = None
    dtCadastro: datetime = None

    idProdutoItem: int = None
    url: str = None
