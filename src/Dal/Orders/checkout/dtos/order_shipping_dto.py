class OrderShippingDto:
    type: str = None
    street: str = None
    number: str = None
    complement: str = None
    district: str = None
    postal_code: str = None
    city: str = None
    state: str = None
    country: str = None
